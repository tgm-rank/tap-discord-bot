interface Env {
  // Add KV namespaces, Durable Object bindings, secrets, etc. here
  tgmrankData: KVNamespace
  // DURABLE_OBJECT: DurableObjectNamespace;
  // SECRET: string;
  FRONTEND_URL: string
  TGMRANK_API_URL: string
  RECENT_ACTIVITY_WEBHOOK_LIST: string
}
