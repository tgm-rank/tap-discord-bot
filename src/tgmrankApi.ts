import {
  Game,
  Mode,
  ModeRankingModel,
  Player,
  RecentActivityModel,
} from '@tgmrank/types'

import slugify from 'slugify'

export enum QueryTypes {
  Id = 'id',
  Name = 'name',
  StartsWith = 'startsWith',
}

export interface IdQuery {
  type: QueryTypes.Id
  id: number
}

export interface NameQuery {
  type: QueryTypes.Name
  name: string
}

export interface StartsWithQuery {
  type: QueryTypes.StartsWith
  query: string
}

export type Query = IdQuery | NameQuery

export async function getGames(env: Env): Promise<ReadonlyArray<Game>> {
  const data = await env.tgmrankData.get('/v1/game')
  if (data == null) {
    return []
  }

  return JSON.parse(data)
}

export function getGameModes(
  games: ReadonlyArray<Game>
): ReadonlyArray<{ game: Game; mode: Mode }> {
  const gameModes = []
  for (const game of games) {
    for (const mode of game.modes ?? []) {
      gameModes.push({
        game,
        mode,
      })
    }
  }

  return gameModes
}

export function findGame(
  games: ReadonlyArray<Game>,
  query: Query
): Game | null | undefined {
  switch (query.type) {
    case QueryTypes.Id:
      return games.find((g) => g.gameId == query.id)
    case QueryTypes.Name:
      return games.find(
        (g) => g.shortName.toLowerCase() === query.name.toLowerCase()
      )
    default:
      return null
  }
}

const modeAliases = new Map<number, string[]>()
modeAliases.set(8, ['death-series', 'death-series-of-5'])
modeAliases.set(13, ['tgm-plus'])

modeAliases.set(15, ['master', 'master-classic'])
modeAliases.set(30, ['qualified-master', 'qualified-master-classic'])
modeAliases.set(16, ['shirase', 'shirase-classic'])
modeAliases.set(17, ['easy', 'easy-classic'])
modeAliases.set(18, ['sakura', 'sakura-classic'])
modeAliases.set(19, ['master-secret-grade', 'master-secret-grade-classic'])
modeAliases.set(27, ['shirase-secret-grade', 'shirase-secret-grade-classic'])
modeAliases.set(25, ['big', 'big-classic'])
modeAliases.set(20, ['master-world'])
modeAliases.set(31, ['qualified-master-world'])
modeAliases.set(21, ['shirase-world'])
modeAliases.set(22, ['easy-world'])
modeAliases.set(23, ['sakura-world'])
modeAliases.set(24, ['master-secret-grade-world'])
modeAliases.set(28, ['shirase-secret-grade-world'])
modeAliases.set(26, ['big-world'])

export function getFriendlyModeName(mode: Mode): string {
  return modeAliases.get(mode.modeId)?.[0] ?? slugify(mode.modeName)
}

export async function getPlayers(env: Env): Promise<ReadonlyArray<Player>> {
  const data = await env.tgmrankData.get('/v1/players/ranked')
  if (data == null) {
    return []
  }

  return JSON.parse(data)
}

export function findPlayer(
  players: ReadonlyArray<Player>,
  query: Query
): Player | null | undefined {
  switch (query.type) {
    case QueryTypes.Id:
      return players.find((p) => p.playerId == query.id)
    case QueryTypes.Name:
      return players.find(
        (p) => p.playerName.toLowerCase() === query.name.toLowerCase()
      )
    default:
      return null
  }
}

export async function getPlayerScores(
  env: Env,
  playerId: number
): Promise<ReadonlyArray<ModeRankingModel>> {
  const scores = await env.tgmrankData.get(`/v1/player/${playerId}/scores`)
  if (scores == null) {
    return []
  }

  return JSON.parse(scores)
}

export function findPersonalBestScore(
  scores: ReadonlyArray<ModeRankingModel>,
  gameId: number,
  modeId: number
) {
  // Assumes list is sorted
  return scores.find((s) => s.gameId === gameId && s.modeId === modeId)
}

const seenScoreIdsKey = 'tgmrank-bot-seenScoreIds'
export async function getRecentlySeenScoreIds(env: Env): Promise<number[]> {
  const value = await env.tgmrankData.get(seenScoreIdsKey)
  if (value == null) {
    return []
  }

  return value
    .split(',')
    .map(Number)
    .filter((n) => !isNaN(n))
}

export async function setRecentlySeenScoreIds(
  env: Env,
  value: number[]
): Promise<void> {
  return env.tgmrankData.put(seenScoreIdsKey, value.join(','))
}

export interface RecentActivityPair {
  main: RecentActivityModel
  extended: RecentActivityModel
}
export async function enhancedRecentActivity(
  env: Env,
  page: number,
  pageSize: number,
  extendedRankingModes: number[]
): Promise<ReadonlyArray<RecentActivityPair>> {
  const ra = await recentActivity(env, page, pageSize)
  const extendedRa = await recentActivity(
    env,
    page,
    pageSize,
    extendedRankingModes
  )

  return ra.map((activity, index) => {
    const extendedActivity = extendedRa[index]

    if (activity.score.scoreId !== extendedActivity.score.scoreId) {
      throw 'Invalid recent activity alignment'
    }

    return {
      main: activity,
      extended: extendedActivity,
    }
  })
}

export async function recentActivity(
  env: Env,
  page: number,
  pageSize: number,
  modes: number[] = []
): Promise<ReadonlyArray<RecentActivityModel>> {
  const url = new URL('/v1/score/activity', env.TGMRANK_API_URL)
  url.searchParams.append('page', page.toString())
  url.searchParams.append('pageSize', pageSize.toString())
  if (modes.length > 0) {
    const modeIds = modes.join(',')
    url.searchParams.append('modeIds', modeIds)
  }
  url.searchParams.append('scoreStatus', 'pending,verified,accepted')

  const response = await fetch(url.toString())
  return response.json<ReadonlyArray<RecentActivityModel>>()
}

export async function getModeRanking(
  env: Env,
  modeId: number,
  asOf?: string
): Promise<ReadonlyArray<ModeRankingModel>> {
  const url = new URL(`/v1/mode/${modeId}/ranking`, env.TGMRANK_API_URL)

  if (asOf != null) {
    url.searchParams.append('asOf', asOf)
  }

  const response = await fetch(url.toString())
  return response.json<ReadonlyArray<ModeRankingModel>>()
}
