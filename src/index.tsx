import { createHandler } from 'slshx'
import { pb } from './pb'
import { recentActivityReporter } from './recentActivity'

const handler = createHandler({
  // Replaced by esbuild when bundling, see scripts/build.js (do not edit)
  applicationId: SLSHX_APPLICATION_ID,
  applicationPublicKey: SLSHX_APPLICATION_PUBLIC_KEY,
  applicationSecret: SLSHX_APPLICATION_SECRET,
  testServerId: SLSHX_TEST_SERVER_ID,
  // Add your commands here
  commands: { pb },
})

async function scheduleHandler(
  event: ScheduledEvent,
  env: Env,
  ctx: ExecutionContext
) {
  ctx.waitUntil(recentActivityReporter(event, env, ctx))
}

export default { fetch: handler, scheduled: scheduleHandler }
