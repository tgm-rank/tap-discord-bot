import { ModeTag } from '@tgmrank/types'
import {
  extractAsPromised as fuzzballExtractAsPromised,
  ratio,
  token_set_ratio,
} from 'fuzzball'

import {
  CommandHandler,
  useDescription,
  createElement,
  Message,
  useString,
} from 'slshx'
import { mutatedTake } from './array-utilities'
import {
  getPlayers,
  findPlayer,
  QueryTypes,
  getGames,
  getPlayerScores,
  findGame,
  findPersonalBestScore,
  getGameModes,
} from './tgmrankApi'
import { ScoreEmbed } from './utility'

const modeArgSeparator = ': '
const maxAutoCompleteOptions = 25

// `Env` contains bindings and is declared in types/env.d.ts
export function pb(): CommandHandler<Env> {
  useDescription("Get a player's Personal Best score")
  const playerArg: string = useString<Env>('player', 'Player Name', {
    required: true,
    async autocomplete(interaction, env, ctx) {
      if (playerArg?.length < 1) {
        return []
      }
      const playerList = await getPlayers(env)
      let choices = playerList.map((p) => p.playerName)
      const fuzzed = await fuzzballExtractAsPromised(playerArg, choices, {
        returnObjects: true,
        scorer: ratio,
      })
      choices = fuzzed.map((f) => f.choice)
      return mutatedTake(choices, maxAutoCompleteOptions)
    },
  })

  const modeArg: string = useString<Env>('mode', 'Mode', {
    required: true,
    async autocomplete(interaction, env, ctx) {
      if (playerArg?.length < 1) {
        return []
      }
      const playersList = await getPlayers(env)
      const player = findPlayer(playersList, {
        type: QueryTypes.Name,
        name: playerArg.toLowerCase(),
      })

      if (player == null) {
        return []
      }

      const games = await getGames(env)
      const gameModes = getGameModes(games)

      const playerScores = await getPlayerScores(env, player.playerId)
      const playerModeIds = playerScores.map((s) => s.modeId)

      const playerGameModes = gameModes.filter((gm) =>
        playerModeIds.includes(gm.mode.modeId)
      )

      playerGameModes?.sort((a, b) => {
        const aEvent = a.mode.tags?.includes(ModeTag.Event)
        const bEvent = b.mode.tags?.includes(ModeTag.Event)
        if (aEvent && !bEvent) {
          return 1
        } else if (!aEvent && bEvent) {
          return -1
        }

        return a.mode.sortOrder - b.mode.sortOrder
      })

      let choices = playerGameModes?.map(
        (gm) => `${gm.game.shortName}${modeArgSeparator}${gm.mode.modeName}`
      )

      if (modeArg?.length > 0) {
        const fuzzed = await fuzzballExtractAsPromised(modeArg, choices, {
          returnObjects: true,
          scorer: token_set_ratio,
        })
        choices = fuzzed.map((f) => f.choice)
      }

      return mutatedTake(choices, maxAutoCompleteOptions)
    },
  })

  return async (interaction, env, ctx) => {
    const playersList = await getPlayers(env)
    if (playersList.length === 0) {
      throw new Error('No players list')
    }

    const player = findPlayer(playersList, {
      type: QueryTypes.Name,
      name: playerArg.toLowerCase(),
    })

    const [gameNameInput, ...rest] = modeArg.split(modeArgSeparator)
    const modeNameInput = rest.join(modeArgSeparator)

    try {
      if (player == null) {
        throw new Error('Player not found')
      }

      const games = await getGames(env)
      if (games.length === 0) {
        throw new Error('No game data')
      }

      const game = findGame(games, {
        type: QueryTypes.Name,
        name: gameNameInput,
      })
      if (game == null) {
        throw new Error('Game not found')
      }

      const mode = game?.modes?.find((m) => m.modeName === modeNameInput)
      if (mode == null) {
        throw new Error('Mode not found')
      }

      const scores = await getPlayerScores(env, player.playerId)
      if (scores == null) {
        throw new Error('No player data')
      }

      const score = findPersonalBestScore(scores, game.gameId, mode.modeId)
      if (score == null) {
        throw new Error('Score not found')
      }

      return (
        <Message>
          <ScoreEmbed
            title={`**__${player.playerName}'s ${game.shortName} ${mode.modeName} Personal Best__**`}
            score={score}
            env={env}
          />
        </Message>
      )
    } catch (e) {
      return <Message ephemeral>{e}</Message>
    }
  }
}
