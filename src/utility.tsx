import {
  DeltaDetails,
  ProofModel,
  ProofType,
  ModeRankingModel,
} from '@tgmrank/types'

import { createElement, Embed, Fragment, Field } from 'slshx'

export function RankRaise({ delta }: { delta: DeltaDetails }): string {
  if (delta.rank == null) {
    return ''
  }

  const rankDifference =
    delta.previousRank != null
      ? delta.previousRank - delta.rank
      : delta.playersOffset.length

  return `(+${rankDifference})`
}

export function mdLink(title: string, href: string): string {
  return `[${title}](${href})`
}

export function MdLink({
  title,
  href,
}: {
  title: string
  href: string
}): string {
  return mdLink(title, href)
}

function timeFormat(duration: string): string {
  return duration?.slice(3).slice(0, -1)
}

function proofThumbnailUrl(proofArray: ReadonlyArray<ProofModel>) {
  const proofThumbnailUrls = proofArray
    .map((proof) => {
      if (proof.type === ProofType.Video) {
        if (proof.link.match(/youtu/)) {
          const re =
            /^(https?:\/\/)?((www\.)?(youtube(-nocookie)?|youtube.googleapis)\.com.*(v\/|v=|vi=|vi\/|e\/|embed\/|user\/.*\/u\/\d+\/)|youtu\.be\/)(?<id>[_0-9a-z-]+)/i
          const id = proof.link.match(re)?.groups?.['id']

          if (id != null) {
            return `https://img.youtube.com/vi/${id}/1.jpg`
          }
        }
      } else if (proof.type === ProofType.Image) {
        return proof.link
      }
    })
    .filter((proof) => proof != null)

  return proofThumbnailUrls.length > 0 ? proofThumbnailUrls[0] : undefined
}

function ScoreDetailsField({
  title,
  score,
}: {
  title: string
  score: ModeRankingModel
}) {
  return (
    <Field name={title} inline>
      {score.grade?.gradeDisplay && (
        <>
          **Grade**: {score.grade?.gradeDisplay}{' '}
          {score.grade?.line != null && <>({score.grade.line})</>}
          {'\n'}
        </>
      )}
      {score.level && (
        <>
          **Level**: {score.level}
          {'\n'}
        </>
      )}
      {score.playtime && (
        <>
          **Time**: {timeFormat(score.playtime)}
          {'\n'}
        </>
      )}
      {score.score && (
        <>
          **Score**: {score.score}
          {'\n'}
        </>
      )}
    </Field>
  )
}

export function ScoreEmbed<Children extends any[]>({
  env,
  title,
  score,
  previousScore,
  color,
  children,
}: {
  env: Env
  title: string
  score: ModeRankingModel
  previousScore?: ModeRankingModel
  color?: number
  children?: Children
}) {
  return (
    <Embed
      title={title}
      url={`${env.FRONTEND_URL}/score/${score.scoreId}`}
      thumbnail={
        score.proof != null ? proofThumbnailUrl(score.proof) : undefined
      }
      color={color ?? 0x597ef7}
      footer="TGM Rank @ theabsolute.plus"
    >
      <ScoreDetailsField title="**__Details__**" score={score} />
      {previousScore != null && (
        <ScoreDetailsField
          title="**__Previous Score__**"
          score={previousScore}
        />
      )}
      {score.comment != null && score.comment.length > 0 && (
        <Field name="**__Comment__**">{score.comment}</Field>
      )}
      {score.proof != null && score.proof.length > 0 && (
        <Field name="**__Proof__**">
          {score.proof
            .map((proof, index) =>
              mdLink(`[#${index + 1}: ${proof.type}]`, proof.link)
            )
            .join(' ')}
        </Field>
      )}
      {children}
    </Embed>
  )
}
