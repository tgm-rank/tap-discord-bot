export function mutatedTake<T>(array: Array<T>, n: number): Array<T> {
  array.length = Math.min(array.length, n)
  return array
}
