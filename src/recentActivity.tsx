import {
  Game,
  Mode,
  ModeTag,
  RecentActivityModel,
  ScoreStatus,
} from '@tgmrank/types'
import { createElement, Field, Message, Fragment } from 'slshx'
import { asOrdinal } from './string-utilities'
import {
  enhancedRecentActivity,
  getFriendlyModeName,
  getGames,
  getModeRanking,
  getRecentlySeenScoreIds,
  RecentActivityPair,
  setRecentlySeenScoreIds,
} from './tgmrankApi'
import { MdLink, RankRaise, ScoreEmbed } from './utility'

export function isPersonalBest(recentActivityEntry: RecentActivityModel) {
  return (
    recentActivityEntry.score?.rank != null &&
    (recentActivityEntry.delta?.mode?.rank != null ||
      recentActivityEntry.previousScore == null)
  )
}

async function pushMessage(webHookUrl: string, body: string): Promise<any> {
  return fetch(webHookUrl, {
    method: 'POST',
    body,
    headers: {
      'content-type': 'application/json;charset=UTF-8',
    },
  })
}

function RankUpdateFields({
  recentActivity,
  game,
  mode,
  env,
}: {
  recentActivity: RecentActivityPair
  game: Game
  mode: Mode
  env: Env
}) {
  return (
    <>
      <Field name="**__Overall*__**" inline>
        <MdLink
          title={
            asOrdinal(recentActivity.main.delta.overall.rank) ?? 'Unranked'
          }
          href={`${env.FRONTEND_URL}/overall`}
        />{' '}
        <RankRaise delta={recentActivity.main.delta.overall} />
      </Field>
      <Field name="**__Game*__**" inline>
        <MdLink
          title={asOrdinal(recentActivity.main.delta.game.rank) ?? 'Unranked'}
          href={`${env.FRONTEND_URL}/${game.shortName}`}
        />{' '}
        <RankRaise delta={recentActivity.main.delta.game} />
      </Field>
      <Field name="**__Mode__**" inline>
        <MdLink
          title={asOrdinal(recentActivity.main.delta.mode.rank) ?? 'Unranked'}
          href={`${env.FRONTEND_URL}/${game.shortName}/${getFriendlyModeName(
            mode
          )}`}
        />{' '}
        <RankRaise delta={recentActivity.main.delta.mode} />
      </Field>
      <Field name="**__Overall+__**" inline>
        <MdLink
          title={
            asOrdinal(recentActivity.extended.delta.overall.rank) ?? 'Unranked'
          }
          href={`${env.FRONTEND_URL}/overall/extended`}
        />{' '}
        <RankRaise delta={recentActivity.extended.delta.overall} />
      </Field>{' '}
      <Field name="**__Game+__**" inline>
        <MdLink
          title={
            asOrdinal(recentActivity.extended.delta.game.rank) ?? 'Unranked'
          }
          href={`${env.FRONTEND_URL}/${game.shortName}/extended`}
        />{' '}
        <RankRaise delta={recentActivity.extended.delta.game} />
      </Field>
      <Field name={'\u200b'} inline>
        {'\u200b'}
      </Field>
    </>
  )
}

function RecentActivityEmbed({
  recentActivity,
  game,
  mode,
  env,
}: {
  recentActivity: RecentActivityPair
  game: Game
  mode: Mode
  env: Env
}) {
  return (
    <ScoreEmbed
      title={`**__${recentActivity.main.score.player?.playerName} did a thing in ${game.shortName} ${mode.modeName}!__**`}
      score={recentActivity.main.score}
      previousScore={recentActivity.main.previousScore}
      env={env}
    >
      <RankUpdateFields
        recentActivity={recentActivity}
        game={game}
        mode={mode}
        env={env}
      />
    </ScoreEmbed>
  )
}

async function carnivalData(
  recentActivity: RecentActivityPair,
  mode: Mode,
  env: Env
): Promise<ReadonlyArray<number>> {
  const carnivalRanking = await getModeRanking(
    env,
    mode.modeId,
    recentActivity.main.score.createdAt
  )
  const total = carnivalRanking.reduce(
    (sum, item) => sum + (item.level ?? 0),
    0
  )

  let offset: number
  if (isPersonalBest(recentActivity.main)) {
    offset =
      recentActivity.main.previousScore?.level != null &&
      recentActivity.main.score.level != null
        ? recentActivity.main.score.level -
          recentActivity.main.previousScore.level
        : recentActivity.main.score.level ?? 0
  } else {
    offset = 0
  }

  return [total, offset]
}

function CarnivalRecentActivityEmbed({
  recentActivity,
  game,
  mode,
  carnivalTotals,
  env,
}: {
  recentActivity: RecentActivityPair
  game: Game
  mode: Mode
  carnivalTotals: ReadonlyArray<number> | null
  env: Env
}) {
  const [total, offset] = carnivalTotals ?? [0, 0]

  return (
    <ScoreEmbed
      title={`__**${recentActivity.main.score.player?.playerName} has added to the ${mode.modeName} DEATH TOLL!**__`}
      color={0xfb3640}
      score={recentActivity.main.score}
      previousScore={recentActivity.main.previousScore}
      env={env}
    >
      ** :skull_crossbones: DEATH TOLL :skull_crossbones: **{'\n'}** :fire:{' '}
      {total} (+{offset}) :fire: **
      <RankUpdateFields
        recentActivity={recentActivity}
        game={game}
        mode={mode}
        env={env}
      />
    </ScoreEmbed>
  )
}

export async function recentActivityReporter(
  event: ScheduledEvent,
  env: Env,
  ctx: ExecutionContext
): Promise<any> {
  const recentScoreIds = new Set(await getRecentlySeenScoreIds(env))

  const games = await getGames(env)
  const recentActivity = await enhancedRecentActivity(
    env,
    1,
    5,
    [1, 2, 3, 4, 29, 12, 6, 7, 13, 11, 9, 14, 10, 15, 16, 17, 18, 25, 19, 30]
  )

  // Seed data if necessary
  if (recentScoreIds.size === 0) {
    for (const p of recentActivity) {
      recentScoreIds.add(p.main.score.scoreId)
    }

    await setRecentlySeenScoreIds(env, Array.from(recentScoreIds))
    return
  }

  const recentActivityToReport = recentActivity
    .filter((p) => !recentScoreIds.has(p.main.score.scoreId))
    .filter(
      (p) =>
        p.main.score.status !== ScoreStatus.Legacy &&
        p.main.score.status !== ScoreStatus.Rejected
    )
    .reverse()

  if (recentActivityToReport.length === 0) {
    return
  } else {
    const scoreIdsToReport = recentActivityToReport.map(
      (p) => p.main.score.scoreId
    )
    console.log(
      `Reporting activity for the following scoreIds: ${scoreIdsToReport}`
    )
  }

  let embeds = []
  for (const p of recentActivityToReport) {
    const game = games.find((g) => g.gameId === p.main.score.gameId)
    const mode = game?.modes?.find((m) => m.modeId === p.main.score.modeId)

    if (game == null) {
      throw new Error(`Could not find game with id ${p.main.score.gameId}`)
    }

    if (mode == null) {
      throw new Error(`Could not find mode with id ${p.main.score.modeId}`)
    }

    const isCarnival = mode.tags?.includes(ModeTag.Carnival)

    // slshx's jsx doesn't play nice with async "components", so now we're getting the data outside of the Carnival embed
    const carnivalTotals = isCarnival ? await carnivalData(p, mode, env) : null

    embeds.push(
      isCarnival ? (
        <CarnivalRecentActivityEmbed
          recentActivity={p}
          game={game}
          mode={mode}
          carnivalTotals={carnivalTotals}
          env={env}
        />
      ) : (
        <RecentActivityEmbed
          recentActivity={p}
          game={game}
          mode={mode}
          env={env}
        />
      )
    )
  }

  const recentActivityMessage = <Message>{embeds}</Message>

  const webHooks = env.RECENT_ACTIVITY_WEBHOOK_LIST.split(',')
  const promises = webHooks.map((webHook) =>
    pushMessage(webHook, JSON.stringify(recentActivityMessage))
  )

  await Promise.allSettled(promises)

  for (const p of recentActivity) {
    recentScoreIds.add(p.main.score.scoreId)
  }

  await setRecentlySeenScoreIds(env, Array.from(recentScoreIds))

  return
}
